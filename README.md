# Ansible Notes #
https://docs.ansible.com/

### Ad-Hoc Modules
###### Show non-default configs
ansible-config dump --only-changed

###### List hosts in inventory
ansible all --list-hosts
ansible-inventory -y --list
ansible-inventory -i inventory --list
ansible web01 --list-hosts

###### Ping all hosts in inventory
ansible all -m ping

###### Find valid privilege escalation choices
ansible-doc -t become -l

###### Add a user
ansible -m user -a 'name=newbie uid=4000 state=present' web01
ansible webservers -m user -a "name=test password=secure state=absent"

###### Remove a user
ansible -m user -a 'name=newbie uid=4000 state=absent' web01

###### Add group - do not use '-'s in group names and be unique from hosts
###### groups can contain network ranges
ansible webservers -m group -a 'name=demo'
ansible webservers -m user -a 'name=test group=demo'

###### List ad-hoc cmds
ansible-doc -l

###### man page for module
ansible-doc ping

###### specify different inventory
ansible all -i inventory -m ping

###### Restart a service
ansible all -m service -a "state=restarted name=sshd"

###### Make sure service is running, start if not
ansible all -m service -a "state=started name=sshd"

###### Manage packages
ansible-doc package

###### Download file 
ansible-doc get_url 

###### Manage networking
ansible-doc nmcli 

###### Interact with api's
ansible-doc uri 

###### Tools
copy
file
lininfile
synchonize
package: ansible webservers -m package -a 'name=httpd state=present'
* yum
* dnf
* gem
system
* firewalld
* reboot
* service
* user
Net Tools:
* get_url
* nmcli
* package

###### Command Module Examples
* command: ansible webservers -m command -a uptime
* shell: ansible webservers -m shell -a 'echo test > test.txt'
* raw: for devices without python

### Playbooks
###### Syntax checker (verifies spaces)
ansible-playbook --syntax-check block_rescue_always.yml

###### Dry Run
ansible-playbook -C example.yml

###### Verbose Logging Levels
-v : includes which config file is in use
-vv : includes version info and handler metadata
-vvv : includes connection info and pretty print json output
-vvvv : includes full connection detail info in output
-vvvvv : includes windows host interactions through winrm

example: ansible-playbook check_mode_yum.yml -vvvv

###### Enable debugger inline
ANSIBLE_ENABLE_TASK_DEBUGGER=True ansible-playbook your-file.yml

Examples:
ansible-playbook step.yml

[web01] TASK: Install a pkg (debug)> p task.args
{'name': 'does_not_exist', 'state': 'present'}
[web01] TASK: Install a pkg (debug)> task.args['name'] = 'bash'
[web01] TASK: Install a pkg (debug)> p task.args
{'name': 'bash', 'state': 'present'}
[web01] TASK: Install a pkg (debug)> redo

or

[web01] TASK: Install a pkg (debug)> p task_vars['pkg_name']
'does_not_exist'
[web01] TASK: Install a pkg (debug)> task_vars['pkg_name'] = 'bash'
[web01] TASK: Install a pkg (debug)> p task_vars['pkg_name']
'bash'
[web01] TASK: Install a pkg (debug)> update_task
[web01] TASK: Install a pkg (debug)> redo

###### Ansible Lint
brew install ansible-lint
ansible-lint
or
ansible-lint yourfile.yml

Skip a rule
ansible-lint -T
ansible-lint -x 403

###### Override Variables - don't use '-'s, use '_'s
ansible-playbook example.yml -e "username=student"

### Secrets 
###### Encrypt Single Variable from stdin and paste in playbook
ansible-vault encrypt_string password123 --ask-vault-pass
ansible-playbook docker_login.yml --ask-vault-pass

###### Encrypt a file
ansible-vault encrypt secret.file

###### Decrypt a file
ansible-vault decrypt secret.file

###### Edit an encrypted file
ansible-vault edit secret.file

###### View an encrypted file
ansible-vault view secret.file

###### Re-Key a file (change password)
ansible-vault rekey secret.file

###### Run playbook with secret
ansible-playbook example.yml --ask-vault-pass


###### List Lookup Plugins
ansible-doc -t lookup -l
ansible-doc -t lookup k8s

###### ansible-galaxy
ansible-galaxy init users       # create skeleton dirs/files for new role
ansible-galaxy search           # search for roles from community site
ansible-galaxy info             # info about a specific role
# https://galaxy.ansible.com/docs/using/installing.html
ansible-galaxy install -p roles # install role from community site or git
ansible-galaxy install -r roles/requirements.yml #install role with requirements
ansible-galaxy remove -p roles encron.vsftpd # remove role

###### Dynamic Inventory
ansible-doc -t inventory -l
ansible-doc -t inventory <plugin name>

###### AWS setup
pip3.8 install ansible boto boto3

###### AWS Dynamic Inventory scripts
https://raw.githubusercontent.com/ansible/ansible/stable-2.9/contrib/inventory/ec2.py
https://raw.githubusercontent.com/ansible/ansible/stable-2.9/contrib/inventory/ec2.ini

###### AWS Ping hosts
ln -s ../ansible.cfg ansible.cfg
ansible -i ec2.py -m ping all
ansible -i ec2.py --limit "tag_env_dev" -m ping all
ansible -i ec2.py --limit "tag_env_dev:&tag_Name_new_demo_template" -m ping all

###### AWS Dynamic Inventory Plugin
# Update ansible.cfg inventory to point at aws_ec2.yml
# Disable debugger for down hosts
ansible-inventory -i aws_ec2.yml --graph
ansible-inventory -i aws_ec2.yml --list
ansible all -i aws_ec2.yml -m ping
ansible -i aws_ec2.yml dev -m ping
ansible -i aws_ec2.yml tag_Name_new_demo_template -m ping

