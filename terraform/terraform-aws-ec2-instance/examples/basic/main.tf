provider "aws" {
  region = "us-west-2"
}

locals {
  user_data = <<EOF
#!/bin/bash
echo "Hello Terraform!"
EOF
}

##################################################################
# Data sources to get VPC, subnet, security group details
##################################################################
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "example"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["104.175.81.15/32"]
  ingress_rules       = ["http-80-tcp", "all-icmp", "https-443-tcp", "ssh-tcp"]
  egress_rules        = ["all-all"]
}

module "ec2_web01" {
  source = "../../"
  instance_count = 1
  name          = "web01"
  ami = "ami-0bc06212a56393ee1"
  instance_type = "t2.micro"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  key_name      = "osx-lereta"
}

module "ec2_web02" {
  source = "../../"
  instance_count = 1
  name          = "web02"
  ami = "ami-0bc06212a56393ee1"
  instance_type = "t2.micro"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  key_name      = "osx-lereta"
}

module "ec2_db01" {
  source = "../../"
  instance_count = 1
  name            = "db01"
  ami = "ami-0bc06212a56393ee1"
  instance_type   = "t2.micro"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  key_name      = "osx-lereta"
}

module "ec2_db02" {
  source = "../../"
  instance_count = 1
  name            = "db02"
  ami = "ami-0bc06212a56393ee1"
  instance_type   = "t2.micro"
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.this_security_group_id]
  key_name      = "osx-lereta"
}
